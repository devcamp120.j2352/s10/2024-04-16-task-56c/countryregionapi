package com.devcamp.api.countryregionapi.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.api.countryregionapi.models.Country;

@Service
public class CountryService {
    @Autowired
    RegionService regionService;

    public ArrayList<Country> getAllCountries() {
        //RegionService regionService = new RegionService();
        ArrayList<Country> countries = new ArrayList<>();
        Country vietnam = new Country("vn", "Việt Nam");
        vietnam.setRegions(regionService.getVietnameseRegions());
        countries.add(vietnam);

        Country china = new Country("cn", "China", regionService.getChineseRegions());
        countries.add(china);

        Country usa = new Country();
        usa.setCountryCode("us");
        usa.setCountryName("America");
        usa.setRegions(regionService.getUsaRegions());
        countries.add(usa);

        return countries;
    }

    public Country getCountryByCountryCode(String countryCode)  {
        ArrayList<Country> countries = getAllCountries();
        Country result = new Country();

        for (Country country : countries) {
            if (country.getCountryCode().equalsIgnoreCase(countryCode)) {
                result = country;
                break;
            }
        }

        return result;
    }
}
